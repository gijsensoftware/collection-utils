import { TypeGuard } from '@gijsensoftware/typescript-guards';

export const filterByType = <T>(input: any[],
                                typeGuard: TypeGuard<T>): T[] => {
    return input.filter(item => typeGuard(item));
}
