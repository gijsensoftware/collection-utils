export interface KeyValuePair {
    key: string,
    value: string
}

export const lastDistinctKvp = () => {
    return (value: KeyValuePair, index: number, self: KeyValuePair[]): boolean => {
        const key = value.key;
        const highestIndexForKey = self
            .map((item, itemIndex) => ({key: item.key, index: itemIndex}))
            .filter((index) => index.key === key)
            .map((itemIndex) => itemIndex.index)
            .reduce((a, b) => a > b ? a : b, -1);
        return highestIndexForKey === index;
    };
};
