import { KeyValuePair, lastDistinctKvp } from './lastDistinctKvp';

describe('lastDistinctKvp', () => {
    const kvpA1: KeyValuePair = {
        key: 'A',
        value: '1',
    };
    const kvpA2: KeyValuePair = {
        key: 'A',
        value: '2',
    };
    const kvpB1: KeyValuePair = {
        key: 'B',
        value: '1',
    };
    const kvpB2: KeyValuePair = {
        key: 'B',
        value: '2',
    };
    const kvpC1: KeyValuePair = {
        key: 'C',
        value: '1',
    };

    it('should return true when given the last kvp for a key', () => {
        const kvpFilter = lastDistinctKvp();

        expect(kvpFilter(kvpA1, 0, [kvpA1, kvpB2]))
            .toBeTruthy();

        expect(kvpFilter(kvpA2, 1, [kvpA1, kvpA2]))
            .toBeTruthy();
    });

    it('should return false when given any but the last kvp for a key', () => {
        const kvpFilter = lastDistinctKvp();

        expect(kvpFilter(kvpA1, 0, [kvpA1, kvpA2]))
            .toBeFalsy();
    });

    it('should be usable in a filter action on an array', () => {
        const kvpFilter = lastDistinctKvp();
        const list = [
            kvpA1,
            kvpA2,
            kvpA1,
            kvpB2,
            kvpC1,
            kvpB1,
            kvpB2,
        ];
        expect(list.filter(kvpFilter))
            .toStrictEqual([
                kvpA1,
                kvpC1,
                kvpB2,
            ]);
    });
});
