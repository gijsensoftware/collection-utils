import {
    isAnyOf,
    isNonNullNonArrayObject,
    isNumber,
    isString,
    valuesFromEnum,
} from '@gijsensoftware/typescript-guards';
import { filterByType } from './filterByType';

enum TestStringEnum {
    Foo = 'Foo',
    Bar = 'Bar'
}

const isTestStringEnum = (input: unknown): input is TestStringEnum =>
    isAnyOf(input, valuesFromEnum(TestStringEnum));

enum TestNumberEnum {
    Zero,
    One
}

const isTestNumberEnum = (input: unknown): input is TestNumberEnum =>
    isAnyOf(input, valuesFromEnum(TestNumberEnum));

describe('filterArray', () => {
    it('should filter out values not matching a type', () => {
        const allKindsOfValuesArray = [
            'a',
            1,
            'b',
            TestStringEnum.Foo,
            TestNumberEnum.Zero,
            {},
            true,
            TestStringEnum.Bar,
            TestNumberEnum.One,
        ];

        expect(filterByType(allKindsOfValuesArray, isString))
            .toEqual([
                'a',
                'b',
                'Foo',
                'Bar',
            ]);

        expect(filterByType(allKindsOfValuesArray, isNumber))
            .toEqual([
                1,
                0,
                1,
            ]);

        expect(filterByType(allKindsOfValuesArray, isNonNullNonArrayObject))
            .toEqual([
                {},
            ]);

        expect(filterByType(allKindsOfValuesArray, isTestStringEnum))
            .toEqual([
                TestStringEnum.Foo,
                TestStringEnum.Bar,
            ]);

        expect(filterByType(allKindsOfValuesArray, isTestNumberEnum))
            .toEqual([
                TestNumberEnum.One,
                TestNumberEnum.Zero,
                TestNumberEnum.One,
            ]);
    });
});
